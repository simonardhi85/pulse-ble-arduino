import * as React from 'react';
import { View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-ionicons'

import HomeScreen from './src/screens/HomeScreen';
import HealthScreen from './src/screens/HealthScreen';

const Tab = createBottomTabNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;

                        if (route.name === 'Home') {
                            iconName = focused ? 'md-home' : 'home';
                        } else if (route.name === 'Health') {
                            iconName = focused ? 'fitness' : 'fitness-outline';
                        }
                        return <Icon ios={iconName} android={iconName} size={size} color={color} />;
                    },
                })}
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'gray',
                }}
            >
                <Tab.Screen name="Home" component={HomeScreen} />
                <Tab.Screen name="Health" component={HealthScreen} />
            </Tab.Navigator>
        </NavigationContainer>
    )
}