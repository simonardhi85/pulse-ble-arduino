import * as React from 'react';
import {Text, View, StyleSheet, Image, Dimensions, FlatList} from 'react-native';
import {
    LineChart
} from 'react-native-chart-kit';
import FusionCharts from 'react-native-fusioncharts';


import LogsCard from '../components/body/cards/LogsCard';
const line = {
    labels: ['0s','1s', '2s', '3s', '4s', '5s', '6s'],
    datasets: [
        {
            data: [0, 550, 50, 150, 45, 50, 50, 550, 50, 150, 45, 500, 50, 0, 550, 50, 150, 45, 500, 50, 0, 550, 50, 150, 45, 500, 50, 0, 550, 50, 150, 45, 500, 50, 0, 550, 50, 150, 45, 500, 50],
            strokeWidth: 2
        },
    ],
};

const DATA = [
    {
        id: 1,
        time: '2020-02-13 00:00:01',
        message: 'Test 1'
    },
    {
        id: 2,
        time: '2020-02-13 00:00:02',
        message: 'Test 2'
    },
    {
        id: 3,
        time: '2020-02-13 00:00:03',
        message: 'Test 3'
    }
];

const dataSource = {
    chart: {
        caption: 'Business Results 2005 v 2006',
        xaxisname: 'Month',
        yaxisname: 'Revenue',
        showvalues: '1',
        numberprefix: '$',
        useroundedges: '1',
        animation: '1'
    },
    categories: [
        {
            category: [
                {
                    label: 'Jan'
                },
                {
                    label: 'Feb'
                },
                {
                    label: 'Mar'
                },
                {
                    label: 'Apr'
                },
                {
                    label: 'May'
                },
                {
                    label: 'Jun'
                },
                {
                    label: 'Jul'
                },
                {
                    label: 'Aug'
                },
                {
                    label: 'Sep'
                },
                {
                    label: 'Oct'
                },
                {
                    label: 'Nov'
                },
                {
                    label: 'Dec'
                }
            ]
        }
    ],
    dataset: [
        {
            seriesname: '2006',
            data: [
                {
                    value: '21000'
                },
                {
                    value: '29800'
                },
                {
                    value: '25800'
                },
                {
                    value: '26800'
                },
                {
                    value: '29600'
                },
                {
                    value: '32600'
                },
                {
                    value: '31800'
                },
                {
                    value: '36700'
                },
                {
                    value: '29700'
                },
                {
                    value: '31900'
                },
                {
                    value: '34800'
                },
                {
                    value: '24800'
                }
            ]
        },
        {
            seriesname: '2005',
            data: [
                {
                    value: '10000'
                },
                {
                    value: '6000'
                },
                {
                    value: '12500'
                },
                {
                    value: '15000'
                },
                {
                    value: '11000'
                },
                {
                    value: '9800'
                },
                {
                    value: '11800'
                },
                {
                    value: '19700'
                },
                {
                    value: '21700'
                },
                {
                    value: '21900'
                },
                {
                    value: '22900'
                },
                {
                    value: '20800'
                }
            ]
        }
    ],
    events: {
        drawComplete: function() {
            console.log('drawn');
        }
    }
};

export default class HealthScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            type: 'msbar2d',
            width: '100%',
            height: '100%',
            dataFormat: 'json',
            dataSource: dataSource,
            schemaJson: null,
            dataJson: null,
            events: {
                beforeRender: function(e, o) {
                    console.log('before render', e, o);
                },
                drawComplete: function(e, o) {
                    console.log('drawn', e, o);
                },
                dataPlotClick: function(e, o) {
                    console.log('First type of Click', e, o);
                }
            }
        };

        this.libraryPath = Platform.select({
            // Specify fusioncharts.html file location
            ios: require('./assets/fusioncharts.html'),
            android: { uri: 'file:///android_asset/fusioncharts.html' }
        });
    }

    componentDidMount() {
        // this.fetchDataAndSchema();
    }

    fetchDataAndSchema() {
        const jsonify = res => res.json();
        const dFetch = fetch(
            'https://s3.eu-central-1.amazonaws.com/fusion.store/ft/data/line-chart-with-time-axis-data.json'
        ).then(jsonify);
        // This is the remote url to fetch the schema.
        const sFetch = fetch(
            'https://s3.eu-central-1.amazonaws.com/fusion.store/ft/schema/line-chart-with-time-axis-schema.json'
        ).then(jsonify);
        Promise.all([dFetch, sFetch]).then(res => {
            const data = res[0];
            const schema = res[1];
            this.setState({ dataJson: data, schemaJson: schema });
        });
    }

    state = {
        firstQuery: '',
    };
    renderItem(data) {
        return (
            <View style={styles.item}>
                <text style={styles.idText}>{data.id}</text>
                <text style={styles.timeText}>{data.time}</text>
                <text style={styles.messageText}>{data.message}</text>
            </View>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.title}>Health</Text>
                    <Image
                        source={require('../assets/img/pulse-logo.png')}
                        style={styles.logoImage}
                    />
                </View>
                <View style={styles.bodyContainer}>
                    <View style={styles.infoContainer}></View>
                    <View style={styles.graphContainer}>
                        <FusionCharts
                            type={this.state.type}
                            width={this.state.width}
                            height={this.state.height}
                            dataFormat={this.state.dataFormat}
                            dataSource={this.state.dataSource}
                            events={this.state.events}
                            libraryPath={this.libraryPath} // set the libraryPath property
                        />
                        {/*<LineChart
                            data={line}
                            width={Dimensions.get('window').width - 15}
                            height={200}
                            yAxisLabel={''}
                            chartConfig={{
                                backgroundColor: '#000',
                                backgroundGradientFrom: 'lightgreen',
                                backgroundGradientTo: '#ffa726',
                                fillShadowGradientOpacity: 0,
                                decimalPlaces: 0, // optional, defaults to 2dp
                                color: (opacity = 0) => `rgba(0, 0, 0, ${opacity})`,
                                style: {
                                    borderRadius: 5
                                },
                                fromZero: true
                            }}
                            style={{
                                marginVertical: 0,
                                borderRadius: 10
                            }}
                        />*/}
                    </View>
                    <View style={styles.logsContainer}>
                        <FlatList
                            data={DATA}
                            renderItem={({data}) => this.renderItem(data)}
                            keyExtractor={data => data.id}
                        />s
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: '#FFFFFF'
    },
    headerContainer: {
        height: 58,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,
        borderBottomWidth: 2,
        borderBottomColor: '#ECECEC'
    },
    title: {
        flex: 1,
        fontSize: 28,
        fontWeight: 'bold',
        marginTop: 10
    },
    logoImage: {
        height: 38,
        width: 100,
        marginTop: 5
    },
    bodyContainer: {
        flex: 3,
        backgroundColor: '#FEFEFE',
        padding: 5
    },
    infoContainer: {
        margin: 2,
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        flex: 1,
        borderColor: '#DFDFDF'
    },
    graphContainer: {
        margin: 2,
        borderWidth: 1,
        borderRadius: 10,
        flex: 2,
        borderColor: '#DFDFDF'
    },
    logsContainer: {
        margin: 2,
        borderWidth: 1,
        borderRadius: 10,
        flex: 2,
        borderColor: 'lightgreen',
        backgroundColor: 'black'
    }
});