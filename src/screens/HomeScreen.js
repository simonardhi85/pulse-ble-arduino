import * as React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

export default class HomeScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        source={require('../assets/img/pulse-logo.png')}
                        style={styles.logoImage}
                    />
                    <View style={styles.subContainer}>
                        <View style={styles.greetingContainer}>
                            <Text style={styles.greetingText}>Hi!</Text>
                            <Text style={styles.nameText}>Simon AY</Text>
                        </View>
                        <View style={styles.accountImageContainer}>
                            <Image
                                source={require('../assets/img/account-512.png')}
                                style={styles.accountImage}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: '#FFFFFF',
        borderBottomWidth: 2,
        borderBottomColor: '#ECECEC',
        padding: 2
    },
    subContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: '#ECECEC',
        padding: 2,
        paddingTop: 25,
        height: 190
    },
    logoImage: {
        height: 50,
        width: 100,
        resizeMode: 'contain',
        marginLeft: 15
    },
    greetingContainer: {
        flex: 1,
        padding: 10
    },
    greetingText: {
        fontSize: 27,
        padding: 5
    },
    nameText: {
        fontSize: 27,
        padding: 5
    },
    accountImageContainer: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 1,
        borderColor: '#CCC',
        backgroundColor: 'white',
        marginTop: 20,
        marginRight: 10
    },
    accountImage: {
        width: 68,
        height: 68
    }

});