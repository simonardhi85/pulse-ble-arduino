import React from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';

export default class LogsCard extends React.Component {
    renderItem(item) {
        return (
            <View style={styles.item}>
                <text style={styles.idText}>{data.id}</text>
                <text style={styles.timeText}>{data.time}</text>
                <text style={styles.messageText}>{data.message}</text>
            </View>
        )
    }

    render() {
        let rowData = this.props.datas;
        return (
            <FlatList
                data={rowData}
                renderItem={({data}) => this.renderItem(data)}
                keyExtractor={data => data.id}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 5,
        backgroundColor: '#000',
    },
    item: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    idText: {
        fontSize: 12,
        color: '#FFF'
    },
    timeText: {
        fontSize: 12,
        color: '#FFF',
    },
    messageText: {
        fontSize: 12,
        color: '#FFF',
        fontWeight: 'bold'
    },
});
