import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
} from 'react-native';

import Images from '@assets/Images';
const window = Dimensions.get('window');

class DefaultHeader extends Component {

    render() {
        return (
            <View style={styles.headerContainer}>
                <View style={styles.logoContainer}>
                    <Images source={Images.imgLogo}/>
                </View>
                <View style={styles.bodyContainer}>
                    <View style={styles.textContainer}>
                        <Text style={styles.textTitle}>Hi!</Text>
                        <Text style={styles.textBody}>Simon A.Y</Text>
                    </View>
                    <View style={styles.logoContainer}>
                        <Images source={Images.accountLogo} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        flex: 1,
        backgroundColor: '#F6F6F6',
        width: window.width,
        height: 250,
    },
    logoContainer: {
        alignContent: 'flex-start',
        height: 50,
    },
    bodyContainer: {
        flexDirection: 'row',
    },
    textContainer: {
        flex: 3
    },
    textTitle: {
        fontSize: 25,
    },
    textBody: {
        fontSize: 27,
        fontWeight: 'bold'
    },
    iconContainer: {
        flex: 1
    }
});

export default DefaultHeader;