const images = {
    imgLogo: require('./img/pulse-logo.png'),
    appLogo: require('./img/logo.jpeg'),
    accountLogo: require('./img/account-512.png')
};

export default images;