import BleManager from 'react-native-ble-manager';
import * as React from "react";

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class BleService extends React.Component {
    handleDisconnectedPeripheral(data) {
        let peripherals = this.state.peripherals;
        let peripheral = peripherals.get(data.peripheral);
        if (peripheral) {
            peripheral.connected = false;
            peripherals.set(peripheral.id, peripheral);
            this.setState({peripherals});
        }
        console.log('Disconnected from  device : ' + data.peripheral);
    }

    handleUpdateValueForCharacteristic(data) {
        console.log(data.value);
        let sensorID = data.value[10];
        switch(sensorID) {
            case 171:
                console.log("Heart Rate = " + data.value[13] + " Bpm")
                break;
            case 178:
                console.log("Blood Oxygen = " + data.value[13] + " %")
                break;
            case 177:
                console.log("Blood Pressure = " + data.value[13] + "/" + data.value[14] + " mmHg")
                break;
            default:
                break;
        }
    }

    handleStopScan() {
        console.log('Scan is stopped');
        this.setState({scanning: false});
    }

    startScan() {
        if (!this.state.scanning) {
            //this.setState({peripherals: new Map()});
            BleManager.scan([], 3, true).then(results => {
                console.log('Scanning...');
                this.setState({scanning: true});
            });
        }
    }

    retrieveConnected() {
        BleManager.getConnectedPeripherals([]).then(results => {
            if (results.length == 0) {
                console.log('No connected peripherals');
            }
            var peripherals = this.state.peripherals;
            for (var i = 0; i < results.length; i++) {
                var peripheral = results[i];
                peripheral.connected = true;
                peripherals.set(peripheral.id, peripheral);
                this.setState({peripherals});
            }
        });
    }

    handleDiscoverPeripheral(peripheral) {
        var peripherals = this.state.peripherals;
        console.log('Got ble peripheral', peripheral);
        if (!peripheral.name) {
            peripheral.name = 'NO NAME';
        }
        peripherals.set(peripheral.id, peripheral);
        this.setState({peripherals});
    }

    doConnect(peripheral) {
        if (peripheral) {
            if (peripheral.connected) {
                BleManager.disconnect(peripheral.id);
            } else {
                BleManager.connect(peripheral.id).then(() => {
                    let peripherals = this.state.peripherals;
                    let p = peripherals.get(peripheral.id);
                    if (p) {
                        p.connected = true;
                        peripherals.set(peripheral.id, p);
                        this.setState({peripherals});
                    }
                    console.log('Connected to ' + peripheral.id);

                    setTimeout(() => {
                        BleManager.retrieveServices(peripheral.id).then(
                            peripheralInfo => {
                                console.log(peripheralInfo);
                                var service = 'c3e6fea0-e966-1000-8000-be99c223df6a';
                                var bakeCharacteristic = 'c3e6fea2-e966-1000-8000-be99c223df6a';
                                var crustCharacteristic = 'c3e6fea2-e966-1000-8000-be99c223df6a';

                                setTimeout(() => {
                                    BleManager.startNotification(
                                        peripheral.id,
                                        service,
                                        bakeCharacteristic,
                                    )
                                        .then(() => {
                                            console.log('Starting get notification from device : ' + peripheral.id);
                                            setTimeout(() => {
                                                BleManager.write(
                                                    peripheral.id,
                                                    service,
                                                    crustCharacteristic,
                                                    [0],
                                                ).then(() => {
                                                    console.log('Send Command to Device');
                                                    BleManager.write(
                                                        peripheral.id,
                                                        service,
                                                        bakeCharacteristic,
                                                        [1, 95],
                                                    ).then(() => {
                                                        console.log('Device Respond');
                                                    });
                                                });
                                            }, 500);
                                        })
                                        .catch(error => {
                                            console.log('Notification error', error);
                                        });
                                }, 200);
                            },
                        );
                    }, 1000);
                })
                .catch(error => {
                    console.log('Connection error', error);
                });
            }
        }
    }
}